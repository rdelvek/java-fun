```java

import java.util.*;  //used for List

public class StreamReduceLambdaExample {
 
    public static void main(String[] args) {
 
        List<String> carArray = Arrays.asList("A honda", "some car honda", "some honda ford");

        boolean containsHonda = allItemsContainText(carArray, "honda"); // true
        boolean containsFord = allItemsContainText(carArray, "ford");   // false

        System.out.println(containsHonda);
        System.out.println(containsFord);
    }

    
    
    /**
        @params: List<String> myList, String someText
        @output: boolean
    */
    public static boolean allItemsContainText(List<String> myList, String someText){
      return myList.stream().allMatch(str -> str.trim().contains(someText));
    }
}

```